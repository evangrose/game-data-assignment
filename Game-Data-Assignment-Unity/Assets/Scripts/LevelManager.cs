﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform coinHolder;

    public enum MapDataFormat
    {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject coinPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> coinPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    //Clears all messages in the console output windows
    static void ClearEditorConsole()
    {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    //Destroys all game objects that are children to the parent object (Level Manager Game Object)
    static void DestroyChildren(Transform parent)
    {
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel()
    {

        //Clears console and destroys all children to restore the level before loading
        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(coinHolder);

        // 
        {
            //Combines file data from external source to a string in the game project
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // 
        {
            //Empties data within the mapData array list
            mapData.Clear();

            //Opens and reads contents of the TMX file and sets  it to the string "content"
            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content)))
            {

                //Reads until an element with the name "map" is found.
                reader.ReadToFollowing("map");
                //Converts object strings to integer values
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));
                //Converts object strings to integer values
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));
                //Converts sheet tile count to an integer and finds the amount of rows and columns that exist within the single tileset
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //Combines file data from external source to a string in the game project
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //Reads until an element with the name "layer" is found.
                reader.ReadToFollowing("layer");

                //Reads until an element with the name "data" is found.
                reader.ReadToFollowing("data");
                //Sets the encoding time from the file read
                string encodingType = reader.GetAttribute("encoding");

                //Alters the encoding type based on the case found, whether its Base64 or CSV within the file
                switch (encodingType)
                {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }
                //Reads all map data as a string value and removes all leading and trailing white-space characters from the element content
                mapDataString = reader.ReadElementContentAsString().Trim();

                //Resets turrent position
                turretPositions.Clear();
                //Read to following point then down to "object" within the file
                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            //Convert object values from file to "x" and "y", and divide them by pixel per unity (resize)
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));
                            //Advances the XmlReader to the next sibling element with the name "object"
                        } while (reader.ReadToNextSibling("object"));
                    }
                }

                //Resets coin position
                coinPositions.Clear();
                //Read to following point then down to "object" within the file
                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            //Convert object values from file to "x" and "y", and divide them by pixel per unity (resize)
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            coinPositions.Add(new Vector3(x, -y, 0));
                            //Advances the XmlReader to the next sibling element with the name "object"
                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }

            //Swap data formats based on case
            switch (mapDataFormat)
            {
                //Set for format Base64
                case MapDataFormat.Base64:
                    //Sets new byte array from the map data found that was set to the string "mapDataString"
                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    //Find and set all tile id's within the index
                    while (index < bytes.Length)
                    {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;

                //Set for format CSV
                case MapDataFormat.CSV:
                    //Sets new string array from the map data found that was set to the string "mapDataString"
                    //Split the data with a space
                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    //Split the line data with a comma
                    foreach (string line in lines)
                    {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        //Find and set all tile id's within the index
                        foreach (string value in values)
                        {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //Set the tile width and height based on the pixel values to a float
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);
            //Offset the tile and map to the center area
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        //Creates new texture from the image loaded and applies a filter and wrap mode
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            //Point filtering - texture pixels become blocky up close.
            spriteSheetTexture.filterMode = FilterMode.Point;
            //Clamps the texture to the last pixel at the edge.
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        //Resets and sets map sprites to a new sprite
        {

            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--)
            {
                for (int x = 0; x < spriteSheetColumns; x++)
                {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        //Resets all tiles and instantiates map tiles sprites to the level
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++)
            {
                for (int x = 0; x < mapColumns; x++)
                {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        //Instantiates all turrets within the index to the turret positon
        {
            foreach (Vector3 turretPosition in turretPositions)
            {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }
        //Instantiates all coin within the index to the coin positon
        {
            foreach (Vector3 coinPosition in coinPositions)
            {
                GameObject coin = Instantiate(coinPrefab, coinPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                coin.name = "Coin";
                coin.transform.parent = coinHolder;
            }
        }

        //Sets date and time to the current time and logs it
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


