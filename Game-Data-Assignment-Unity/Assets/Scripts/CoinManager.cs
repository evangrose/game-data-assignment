﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{

    SpriteRenderer m_SpriteRenderer;
    public bool rendererEnabled;

    private void Awake()
    {
        rendererEnabled = true;
    }


    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        m_SpriteRenderer.enabled = rendererEnabled;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("hit");
            rendererEnabled = false;
        }
    }
}
