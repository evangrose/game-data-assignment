﻿using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 15;
    public float lifetime = 2;
    public float aliveTime = 0;

    Vector3 forward;

    void Start() {
        forward = new Vector3(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad), 0);
    }

    void Update() {
        aliveTime += Time.deltaTime;
        if (aliveTime > lifetime) {
            Destroy(gameObject);
            return;
        }
        Debug.DrawRay(transform.position, forward, Color.blue);
        transform.Translate(forward * speed * Time.deltaTime, Space.World);
    }
}
