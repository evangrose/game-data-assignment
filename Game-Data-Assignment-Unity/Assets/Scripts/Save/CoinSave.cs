﻿using System;
using UnityEngine;

[Serializable]
public class CoinSave : Save
{
    public Data data;
    private CoinManager coin;
    private string jsonString;

    [Serializable]
    //Serializable to save entire class
    //Creating an object of data, setting position, rotation, and click data
    public class Data : BaseData
    {
        public Vector3 position;
        public bool rEnabled;
    }

    void Awake()
    {
        coin = GetComponent<CoinManager>();
        data = new Data();
    }

    //Will return a json string
    //Sets data the corresponding data
    public override string Serialize()
    {
        //Important to match prefab name
        data.prefabName = prefabName;
        //The position
        data.position = coin.transform.position;
        //The boolean
        data.rEnabled = coin.rendererEnabled;

        //Json takes the data and converts it to a string
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData)
    {
        //Takes all of the data to place them in to matching positions
        JsonUtility.FromJsonOverwrite(jsonData, data);
        coin.rendererEnabled = data.rEnabled;
        coin.transform.position = data.position;

        coin.name = "Coin";
    }
}
