﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    //Everything saved goes into this list
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable()
    {
        //When enabled, go to OnSceneLoaded method
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }


    public void AddObject(string item)
    {
        saveItems.Add(item);
    }

    public void Save()
    {
        //Clear saved items list (Wipes it clean) **Important
        saveItems.Clear();

        //Make an array for every saveable item and store it in this array *Must inherit from save
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        //Go through each item and add them to saveable objects
        foreach (Save saveableObject in saveableObjects)
        {
            saveItems.Add(saveableObject.Serialize());
        }

        //Creates the file if it doesn't exist and opens it for us to write to in the backend
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath))
        {
            foreach (string item in saveItems)
            {
                //Input string(s) into the file
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    public void Load()
    {
        //Reloads the whole level
        //Set first play to false
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }



    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //If this is the first time played don't do anything
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    void LoadSaveGameData()
    {
        //Look for a game-save.json file
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath))
        {
            //Does this game file have information
            while (gameDataFileStream.Peek() >= 0)
            {
                //Read first line and trim it
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0)
                {
                    //If it has a length to it, add it to the save items list
                    saveItems.Add(line);
                }
            }
        }
    }

    void DestroyAllSaveableObjectsInScene()
    {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            //Create in the location of the parsed save file (If not could have duplicates) -- Causes duplicate bugs
            Destroy(saveableObject.gameObject);
        }
    }

    void CreateGameObjects()
    {   //Check through the save file
        foreach (string saveItem in saveItems)
        {   //Find the name of the prefab we are spawning with the data saved
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);
            //Spawn the prefab that we read and wrote
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            //Stop writing new data
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
