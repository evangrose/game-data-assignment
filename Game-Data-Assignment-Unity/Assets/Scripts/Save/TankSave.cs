﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    [Serializable]
    //Serializable to save entire class
    //Creating an object of data, setting position, rotation, and click data
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    void Awake()
    {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //Will return a json string
    //Sets data the corresponding data
    public override string Serialize()
    {
        //Important to match prefab name
        data.prefabName = prefabName;
        //The position
        data.position = tank.transform.position;
        //The rotation
        data.eulerAngles = tank.transform.eulerAngles;
        //The click location
        data.destination = tank.destination;
        //Json takes the data and converts it to a string
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData)
    {
        //Takes all of the datato place them in to matching positions
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}